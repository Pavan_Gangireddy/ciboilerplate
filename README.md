Standard compliant React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)

## Post creation of project

Check the respective sections if you have opted or using them

* [**Common Guidelines**](app/docs/COMMON_GUIDELINES.md): Code, Project, PR and Review guidelines
* [**Commands**](app/docs/COMMANDS.md): Getting the most out of this boilerplate
* [**React Native Config**](app/docs/REACT_NATIVE_CONFIG.md): Add your secret keys environemnt wise
* [**StyleSheet**](app/docs/RESPONSIVE_STYLE_SHEET.md): Stylesheet usage guidelines
* [**Lint**](app/docs/LINT.md): Lint guidelines
* [**Sound**](app/docs/SOUND.md): Sound usage guidelines
* [**Analytics**](app/docs/REACT_NATIVE_ANALYTICS.md): Steps to complete analytics setup based on android or ios
* [**Multiple Environments Setup**](app/docs/MULTIPLE_ENVIRONMENTS.md): configuring .env files specific to releases(alpha, beta, etc.,)
* [**S3 Image Uploader**](app/docs/S3_IMAGE_UPLOADER.md): Utils related to s3 image uploader
* [**LogEntries**](app/docs/REACT_NATIVE_LOGENTRIES.md): Steps to complete analytics setup based on android or ios
* [**Base Auth Store**](app/docs/BASE_AUTH_STORE.md): Base Auth Store Configurations

## System Setup

* [**Ubuntu Setup**](app/docs/UBUNTU_REACT_NATIVE_SETUP.md): Steps for setting up react native in your ubuntu system

## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`

## :arrow_forward: How to Run App

1.  cd to the repo
2.  Run Build for either OS

* for iOS
  * run `react-native run-ios`
* for Android
  * Run Genymotion
  * run `react-native run-android`
