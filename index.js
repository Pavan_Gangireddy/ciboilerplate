/*
 * @flow
 */

import { AppRegistry } from 'react-native'
import './app/config/globals'
import App from './app/containers/App'

AppRegistry.registerComponent('CIBoilerplate', () => App)
