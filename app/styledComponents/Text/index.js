/*
 * @flow
 */

import styled from '@rn/styled-components/native'

const BaseText = styled.Text`
  background-color: transparent;
`

export const Typo18W = styled(BaseText)`
  font-size: 18;
  letter-spacing: -0.3;
  color: #ffffff;
`
