/**
 * @flow
 * Author: Maneesh
 **/
import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import { storiesOf } from '@storybook/react-native'

import { Colors } from '../../themes'

import { Typo18W } from './index'
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
    paddingBottom: 30
  }
})
storiesOf('Text', module).add('Typo', () => (
  <ScrollView contentContainerStyle={styles.container}>
    <Typo18W>Typo18W</Typo18W>
  </ScrollView>
))
