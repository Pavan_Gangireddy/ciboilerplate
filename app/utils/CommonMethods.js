/**
 * @flow
 * Author: Prasanth
 * */

import I18n from 'react-native-i18n'
import { Alert, Platform, Linking } from 'react-native'

import {
  INTERNAL_SERVER_ERROR_CODE,
  NO_INTERNET_ERROR_CODE
} from '../constants/ApiErrorConstants'

import type { APIError, ErrorType } from '../stores/types'

export const REQUEST_TIMED_OUT = 'Endpoint request timed out'

export function getCommonErrorDescription() {
  return I18n.t('common.pageUnavailable')
}

export function displayApiError(
  apiError,
  showAlert: boolean = false
): ErrorType {
  let description: string = ''
  let errorConstant: string = ''
  let title: string = I18n.t('common.somethingWentWrong')
  let errorCode: number = INTERNAL_SERVER_ERROR_CODE
  if (apiError) {
    try {
      const { message } = apiError
      const parsedMessage: any = JSON.parse(message)
      if (__DEV__) console.log('displayApiError message', parsedMessage)
      let parsedError: APIError
      if (!parsedMessage.data) {
        // To handle case when we are directly returing backend  error message
        parsedError = parsedMessage
      } else {
        // To handle case when we are adding all the requests to backend error message
        parsedError = parsedMessage.data
      }
      if (parsedError) {
        if (parsedError.message && parsedError.message === REQUEST_TIMED_OUT) {
          title = I18n.t('common.somethingWentWrong')
          description = I18n.t('common.apiRequestTimeout')
        }

        if (parsedError.response) {
          description = parsedError.response
          errorConstant = parsedError.res_status
        }
        if (parsedError.http_status_code) {
          errorCode = parsedError.http_status_code
          errorConstant = parsedError.res_status
          if (parsedError.http_status_code === NO_INTERNET_ERROR_CODE) {
            title = I18n.t('common.connectionLost')
            description = I18n.t('common.yourInternetSeemsToBeOffline')
          }
        }
      }
    } catch (e) {
      if (__DEV__) {
        console.log('displayApiError', e, apiError)
      }
    }
  }

  if (!description) {
    title = I18n.t('common.somethingWentWrong')
    description = getCommonErrorDescription()
  }
  if (showAlert) {
    Alert.alert(title, description)
  }
  const apiErrorResponse = {
    errorCode,
    title,
    description,
    errorConstant
  }
  if (__DEV__) {
    console.log('displayApiError >>', apiErrorResponse)
  }
  return apiErrorResponse
}

export function getErrorMessage(apiError) {
  const errorMessage = displayApiError(apiError)
  if (errorMessage.description === getCommonErrorDescription()) {
    return errorMessage.title
  }
  return errorMessage.description
}

export function gotoUpdateAppStore() {
  const urls = {
    iosLink: 'Enter your ios app link',
    androidLink: 'Enter your android app link'
  }
  const { iosLink, androidLink } = urls
  if (Platform.OS === 'ios') {
    Linking.openURL(iosLink).catch(err =>
      console.error('An error occurred', err)
    )
  } else if (Platform.OS === 'android') {
    Linking.openURL(androidLink).catch(err =>
      console.error('An error occurred', err)
    )
  }
}

export function getFormattedLanguage(language: string) {
  return `${language}-${language}`
}
