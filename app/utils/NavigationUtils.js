/*
 * @flow
 */

import { Actions } from 'react-native-router-flux'
import { APP_LAUNCH_SCREEN, SELECT_LANGUAGE_SCREEN } from '../constants/NavigationConstants'

export function navigate(screenName, props) {
  Actions[screenName](props)
}

export function navigateBack() {
  Actions.pop()
}

export const goToLaunchScreen = (props: object = {}) => {
  const sceneProps = { ...props }
  navigate(APP_LAUNCH_SCREEN, sceneProps)
}

export const goToSelectLanguageScreen = (props: object = {}) => {
  const sceneProps = { ...props }
  navigate(SELECT_LANGUAGE_SCREEN, sceneProps)
}
