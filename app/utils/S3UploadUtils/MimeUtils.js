const mime = require('mime')

export default function getFileType(mimeString) {
  const mediaType = mimeString.split('/')[0].toUpperCase()
  const extension = mime.getExtension(mimeString)
  return {
    mediaType,
    extension
  }
}
