/**
 * @flow
 * */

// INFO: This config Will be updated in S3ImageUploader from API Call
export const S3Config = {
  s3Region: '',
  cognitoRegion: '',
  identityPoolId: '',
  bucketName: '',
  path: '',
  accessKeyId: '',
  secretAccessKey: '',
  awsSessionToken: '',
  downloadKeyName: '',
  uploadKeyName: '',
  cloudFrontDomain: ''
}
