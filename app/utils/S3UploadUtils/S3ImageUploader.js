/*
 * @flow
 * @Author: Prasanth
 */

import ImagePicker from 'react-native-image-crop-picker'
import AWSS3Utility, {
  CREDENTIALS_TYPE_ACCESS_KEY
} from '@rn/aws-sdk/AWSS3Utility'
import DeviceInfo from 'react-native-device-info'

import { S3Config } from './S3Config'
import getFileType from './MimeUtils'
import { imageUploadStatus } from '../../constants/AppConstants'

const pickerOptions = {
  width: 300,
  height: 300,
  cropping: true,
  mediaType: 'photo'
}
class S3ImageUploader {
  constructor(pickerOptions = pickerOptions) {
    this.pickerInstances = 0
    this.pickerOptions = pickerOptions
  }
  resetPickerInstances = () => {
    this.pickerInstances = 0
  }

  uploadImage = (getAwsCredentials: () => mixed, onUpload: () => mixed) => {
    this.pickerInstances += 1
    if (this.pickerInstances === 1) {
      ImagePicker.openPicker(this.pickerOptions)
        .then(async image => {
          this.resetPickerInstances()
          const currentUnixTime = new Date().getTime()
          const uniqueId = DeviceInfo.getUniqueID()
          console.log(
            'Pic selection completed: ',
            image,
            image.path,
            currentUnixTime
          )

          const fileInfo = getFileType(image.mime)
          if (__DEV__) console.log('File Info ===> &', fileInfo)
          // on upload start with local file url
          onUpload({ url: image.path }, imageUploadStatus.uploadingStart)
          try {
            const awsCreds = await getAwsCredentials()
            if (awsCreds) {
              // INFO: S3 Config options are updated from backend
              S3Config.bucketName = awsCreds.bucket_name
              S3Config.path = awsCreds.folder_name
              S3Config.s3Region = awsCreds.region_name
              S3Config.accessKeyId = awsCreds.aws_access_key_id
              S3Config.secretAccessKey = awsCreds.secret_access_key
              S3Config.awsSessionToken = awsCreds.aws_session_token
              S3Config.cloudFrontDomain = awsCreds.cloudfront_domain
            } else {
              onUpload({}, imageUploadStatus.uploadingFailed)
            }
          } catch (e) {
            onUpload({}, imageUploadStatus.uploadingFailed)
          }
          const awsUploader = new AWSS3Utility(
            S3Config,
            CREDENTIALS_TYPE_ACCESS_KEY
          )
          const key = `${
            S3Config.path
          }profilePics/IMG_${currentUnixTime}_${uniqueId}.${fileInfo.extension}`
          const imageUploadResponse = awsUploader.uploadImageFromURI(
            {
              // Body: new Buffer(body, 'base64'),
              path: image.path,
              Key: key,
              Bucket: S3Config.bucketName,
              ACL: 'public-read',
              ContentType: image.mime
            },
            (error, data) => {
              if (error) {
                onUpload({}, imageUploadStatus.uploadingFailed)
              }
              if (__DEV__) console.log('Uploaded to s3', error, data)
            }
          )
          const imageUploadResponsePromise = await imageUploadResponse
          const imageUploadResponseData = await imageUploadResponsePromise.promise()
          if (__DEV__) {
            console.log(
              'Image Upload response data:  &',
              imageUploadResponseData
            )
          }
          // on upload success with s3 url
          onUpload(
            {
              url: S3Config.cloudFrontDomain + key,
              media_type: fileInfo.mediaType
            },
            imageUploadStatus.uploadingCompleted
          )
        })
        .catch(error => {
          if (__DEV__) console.log('In Pic Selction Error ==> ', error)
          this.resetPickerInstances()
        })
    } else {
      // console.log('picker alread opened')
    }
  }
}

const s3ImageUploader = new S3ImageUploader()
export { s3ImageUploader as default, S3ImageUploader }
