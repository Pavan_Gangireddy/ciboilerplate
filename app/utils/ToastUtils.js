/**
 * @flow
 * */
/* eslint-disable react-native/split-platform-components */
import { ToastAndroid, Platform, Alert } from 'react-native'

export function showToast(message: string) {
  if (Platform.OS === 'android') {
    ToastAndroid.show(message, ToastAndroid.SHORT)
  } else {
    Alert.alert('', message)
  }
}
