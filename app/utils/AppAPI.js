/**
 * @flow
 * @author: Prasanth
 */

import getData, { getAuthorizationHeaders } from '@rn/app-api'
import AuthStore from '../stores/AuthStore/BaseAuthStore'
import EnvironmentConstants from '../constants/EnvironmentConstants'

const { SOURCE } = EnvironmentConstants

export const networkCallWithApisauce = async (
  api,
  url,
  requestObject,
  type = 'POST'
) => {
  const { accessToken } = AuthStore

  api.setHeaders(getAuthorizationHeaders(accessToken, SOURCE))
  let response = null
  try {
    response = await getData(api, url, requestObject, type)
  } catch (error) {
    throw error
  }
  return response
}
