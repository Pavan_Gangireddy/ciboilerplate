/*
 * @flow
 */
import '../../config'
import DebugConfig from '../../config/DebugConfig'
import React, { Component } from 'react'
import { Provider, observer } from 'mobx-react/native'
import I18n from 'react-native-i18n'

import RootContainer from '../RootContainer'
import stores from '../../stores'
import { settingsStorePromise } from '../../stores/SettingsStore'

/**
 * Provides an entry point into our application.  Both index.ios.js and index.android.js
 * call this component first.
 *
 * We create our Redux store here, put it into a provider and then bring in our
 * RootContainer.
 *
 * We separate like this to play nice with React Native's hot reloading.
 */
class App extends Component {
  async init() {
    await settingsStorePromise
    const userLanguage = stores.settingsStore.userLanguage
    stores.settingsStore.setUserLanguage(userLanguage)
  }
  componentWillMount() {
    this.init()
  }
  render() {
    return (
      <Provider {...stores}>
        <RootContainer />
      </Provider>
    )
  }
}

// allow reactotron overlay for fast design in dev mode
export default observer(App)
