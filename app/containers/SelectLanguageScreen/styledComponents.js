/**
 * @flow
 * @author: Anesh Parvatha
 * */

import styled from '@rn/styled-components/native'
import Image from '../../components/Common/Image'

export const BackgroundContainer = styled.View``

export const MainContainer = styled.ScrollView`
  padding-left: 20;
  padding-right: 20;
`

export const ConfirmButton = styled.Button`
  flex-direction: row;
`
