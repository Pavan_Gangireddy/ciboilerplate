/**
 * @flow
 * @author: Anesh Parvatha
 * */

import React, { Component } from 'react'
import { observer, inject } from 'mobx-react/native'
import I18n from 'react-native-i18n'
import { API_FETCHING } from '@rn/api-constants'

import Touchable from '../../components/Common/Touchable'

import SelectUserLanguage from '../../components/SelectUserLanguage'

import type { SettingsStore } from '../../stores/SettingsStore'
import type { LanguageStore } from '../../stores/LanguageStore'

import Images from '../../themes/Images'

import { navigateBack } from '../../utils/NavigationUtils'

import {
  BackgroundContainer,
  MainContainer,
  ConfirmButton
} from './styledComponents'
import styles from './styles'
import { showToast } from '../../utils/ToastUtils'
import { updateTranslations } from '../../I18n/I18n'

type Props = {}

type InjectedProps = {
  settingsStore: SettingsStore,
  languageStore: LanguageStore
}

@inject('languageStore', 'settingsStore')
@observer
class SelectLanguageScreen extends Component<Props & InjectedProps> {
  userSettingsModel = this.props.settingsStore.createViewModel()
  constructor(props) {
    super(props)
    console.log('props', props)
  }
  componentDidMount() {
    const { languageStore } = this.props
    languageStore.getLanguagesAPI()
  }
  onSelectLanguage = language => {
    const { settingsStore } = this.props
    const { userLanguage } = this.userSettingsModel

    settingsStore.setUserLanguage.call(this.userSettingsModel, language)
    updateTranslations(userLanguage.languageId)
  }

  onSubmitLanguage = () => {
    const { userLanguage } = this.userSettingsModel
    this.userSettingsModel.submit()
    I18n.locale = userLanguage.languageId
    showToast(I18n.t('selectLanguageScreen.languageUpdateSuccess'))
    navigateBack()
  }

  render() {
    const { languageStore } = this.props
    const selectedLanguage = this.userSettingsModel.userLanguage.languageId
    const confirmText = I18n.t('selectLanguageScreen.confirm', {
      locale: selectedLanguage
    })
    return (
      <BackgroundContainer>
        <MainContainer contentContainerStyle={styles.scrollView}>
          <SelectUserLanguage
            selectedLanguage={selectedLanguage}
            style={styles.selectLanguageStyles}
            onSelectLanguage={this.onSelectLanguage}
            languageStore={languageStore}
          />
          <ConfirmButton onPress={this.onSubmitLanguage} title={confirmText} />
        </MainContainer>
      </BackgroundContainer>
    )
  }
}
export default SelectLanguageScreen
