/**
 * @flow
 * @author: Anesh Parvatha
 * */

import StyleSheet from '../../components/Common/StyleSheet'

const styles = StyleSheet.create({
  selectLanguageStyles: {
    marginTop: 20,
    marginBottom: 20
  },
  touchableStyles: {
    position: 'absolute',
    right: 30,
    top: 30,
    width: 24,
    height: 24
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 30
  }
})

export default styles
