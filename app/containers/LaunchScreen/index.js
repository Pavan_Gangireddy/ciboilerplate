/*
 * @flow
 */
import React, { Component } from 'react'
import { Button, ScrollView, Text, Image, View } from 'react-native'
import I18n from 'react-native-i18n'
import { Images } from '../../themes'

import {
  goToSelectLanguageScreen
} from '../../utils/NavigationUtils'

// Styles
import styles from './styles'

export default class LaunchScreen extends Component {
  onChangeLanguage = () => {
    goToSelectLanguageScreen()
  }
  render() {
    return (
      <View style={styles.mainContainer}>
        <Image
          source={Images.background}
          style={styles.backgroundImage}
          resizeMode="stretch"
        />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.launch} style={styles.logo} />
          </View>

          <Button
            title={I18n.t('launchScreen.changeLanguage')}
            onPress={this.onChangeLanguage}
          />

          <View style={styles.section}>
            <Image source={Images.ready} />
            <Text style={styles.sectionText}>
              {I18n.t('launchScreen.igniteIntroText')}
            </Text>
          </View>
        </ScrollView>
      </View>
    )
  }
}
