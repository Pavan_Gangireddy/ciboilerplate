/*
 * @flow
 */
import React, { Component } from 'react'
import { View, StatusBar } from 'react-native'
import { observer, inject } from 'mobx-react/native'
import NavigationRouter from '../../navigation/NavigationRouter'

// Styles
import styles from './styles'

@inject('settingsStore')
@observer
class RootContainer extends Component {
  render() {
    const {settingsStore} = this.props
    return (
      <View style={styles.applicationView} key={settingsStore.userLanguage.languageId}>
        <StatusBar barStyle="light-content" />
        <NavigationRouter />
      </View>
    )
  }
}

export default RootContainer
