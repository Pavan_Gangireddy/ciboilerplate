/**
 * @flow
 */

import { observable, action, computed } from 'mobx'
import { create, persist } from 'mobx-persist'
import { AsyncStorage } from 'react-native'

const pour = create({
  storage: AsyncStorage
})

class AuthStore {
  @persist
  @observable
  accessToken: string = ''
  refreshToken: string = ''
  source: string = ''

  constructor() {
    this.hydrate()
  }

  @computed
  get isLoggedIn(): Promise<boolean> {
    if (
      this.accessToken === null ||
      this.accessToken === undefined ||
      this.accessToken === ''
    ) {
      return pour('accessToken', this).then(() => {
        if (
          this.accessToken === null ||
          this.accessToken === undefined ||
          this.accessToken === ''
        ) {
          return false
        }
        return true
      })
    }
    return Promise.resolve(true)
  }

  @action.bound
  setAccessToken(accessToken: string): void {
    if (__DEV__) console.log('In setAccessToken ', accessToken)
    this.accessToken = accessToken
  }

  @action.bound
  setRefreshToken(refreshToken: string): void {
    this.refreshToken = refreshToken
  }

  @action.bound
  setSource(source: string): void {
    this.source = source
  }

  @action.bound
  hydrate() {
    Object.keys(this).forEach((key) => {
      pour(key, this)
    })
  }
}

const authStore = new AuthStore()
export { authStore as default, AuthStore }
