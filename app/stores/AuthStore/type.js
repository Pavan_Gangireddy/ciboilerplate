/**
 * @flow
 * @author: Prasanth
 */

export type APIPhoneNumber = {
  phone_number: string,
  country_code: string
}

export type APIVerifyLoginOtpRequest = {
  phone_number: string,
  country_code: string,
  client_secret: string,
  client_id: string,
  otp: string
}

//TODO: Change the profile type according to the project
export type APIVerifyLoginOtpResponse = {
  profile: any,
  user_id: number,
  user_type: string,
  access_token: string,
  refresh_token: string
}
