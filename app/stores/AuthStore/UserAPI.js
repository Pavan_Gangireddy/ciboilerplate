/**
 * @flow
 * @author: Prasanth
 */

import { create } from 'apisauce'
import EnvironmentConstants from '../../constants/EnvironmentConstants'
import { networkCallWithApisauce } from '../../utils/AppAPI'
import urls from '../endpoints'
import type { APIPhoneNumber, APIVerifyLoginOtpRequest } from './type'

const { AUTH_BASE_URL } = EnvironmentConstants

class UserAPI {
  constructor() {
    this.api = create({
      baseURL: AUTH_BASE_URL
    })
  }
  async userLoginApi(requestObject: APIPhoneNumber) {
    const url = urls.auth.userLogin
    return networkCallWithApisauce(this.api, url, requestObject)
  }
  async userLoginOtpVerifyApi(requestObject: APIVerifyLoginOtpRequest) {
    const url = urls.auth.verifyLoginOtp
    return networkCallWithApisauce(this.api, url, requestObject)
  }
  async userLoginOtpResendApi(requestObject: APIPhoneNumber) {
    const url = urls.auth.resendLoginOtp
    return networkCallWithApisauce(this.api, url, requestObject)
  }
  async getUserProfileApi() {
    const url = urls.profile.getUserProfile
    return networkCallWithApisauce(this.api, url, {})
  }
  async userLogOutApi() {
    const url = urls.auth.userLogout
    return networkCallWithApisauce(this.api, url, {})
  }
}

const userAPI = new UserAPI()
export { userAPI as default, UserAPI }
