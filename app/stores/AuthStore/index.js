/**
 * @flow
 * @author: Prasanth
 */

import { AsyncStorage } from 'react-native'
import { observable, action } from 'mobx'
import { bindPromiseWithOnSuccess } from '@rn/mobx-promise'
import { API_INITIAL } from '@rn/api-constants'
import type { APIStatus } from '@rn/api-constants'
import EnvironmentConstants from '../../constants/EnvironmentConstants'
import { displayApiError } from '../../utils/CommonMethods'
import { gotoLaunchScreen } from '../../utils/NavigationUtils'
import userProfile from '../UserProfile'

import BaseAuthStore from './BaseAuthStore'
import UserAPI from './UserAPI'

import type {
  APIPhoneNumber,
  APIVerifyLoginOtpRequest,
  APIVerifyLoginOtpResponse
} from './type'

const { CLIENT_ID, CLIENT_SECRET } = EnvironmentConstants

class AuthStore {
  @observable userLoginStatus: APIStatus
  @observable userLoginVerifyStatus: APIStatus
  @observable userLoginOtpResendStatus: APIStatus
  @observable getProfileStatus: APIStatus
  @observable logOutStatus: APIStatus

  userAPI: any

  constructor(UserAPI: any) {
    this.userAPI = UserAPI
  }

  @action.bound
  setUserProfile(user: APIVerifyLoginOtpResponse) {
    // TODO: Set user profile according to project
  }

  @action.bound
  setUserLoginStatus(status: APIStatus) {
    this.userLoginStatus = status
  }

  @action.bound
  userLoginSuccess() {
    if (__DEV__) console.log('Success')
  }

  @action.bound
  setUserLoginVerifyStatus(status: APIStatus) {
    this.userLoginVerifyStatus = status
  }

  @action.bound
  userLoginVerifySucces(user: APIVerifyLoginOtpResponse) {
    BaseAuthStore.setAccessToken(user.access_token)
    BaseAuthStore.setRefreshToken(user.refresh_token)
    this.setUserProfile(user)
  }

  @action.bound
  resendOtpSuccess() {
    if (__DEV__) console.log()
  }

  @action.bound
  setUserLoginOtpResendStatus(status: APIStatus) {
    this.userLoginOtpResendStatus = status
  }

  @action.bound
  setGetProfileStatus(status: APIStatus) {
    this.getProfileStatus = status
  }

  @action.bound
  setLogOutStatus(status: APIStatus) {
    this.logOutStatus = status
  }

  @action.bound
  setLogOut() {
    AsyncStorage.clear()
  }

  // TODO:Should make country code dynamic//
  userLogin() {
    const requestObject = {
      phone_number: userProfile.phoneNumber,
      country_code: '91'
    }
    this.userLoginApi(requestObject)
  }

  @action.bound
  async userLoginApi(
    requestObject: APIPhoneNumber,
    shouldUpdateImmediately: boolean = true
  ) {
    const userLoginPromise = this.userAPI.userLoginApi(requestObject)
    if (shouldUpdateImmediately) {
      return bindPromiseWithOnSuccess(userLoginPromise)
        .to(this.setUserLoginStatus, this.userLoginSuccess)
        .catch((error) => {
          if (__DEV__) console.log(error)
          displayApiError(error)
        })
    }
    return userLoginPromise
  }

  // TODO:Should make country code dynamic//
  userLoginVerify(otp: string) {
    const requestObject = {
      phone_number: userProfile.phoneNumber,
      country_code: '91',
      client_secret: CLIENT_SECRET,
      client_id: CLIENT_ID,
      otp
    }
    return this.userLoginOtpVerifyApi(requestObject)
  }

  @action.bound
  async userLoginOtpVerifyApi(
    requestObject: APIVerifyLoginOtpRequest,
    shouldUpdateImmediately: boolean = true
  ) {
    const userOtpPromise = this.userAPI.userLoginOtpVerifyApi(requestObject)
    if (shouldUpdateImmediately) {
      return bindPromiseWithOnSuccess(userOtpPromise)
        .to(this.setUserLoginVerifyStatus, this.userLoginVerifySucces)
        .catch((error) => {
          if (__DEV__) console.log(error)
          displayApiError(error)
        })
    }
    return userOtpPromise
  }

  // TODO:Should make country code dynamic//
  resendUserLoginOtp() {
    const requestObject = {
      phone_number: userProfile.phoneNumber,
      country_code: '91'
    }
    this.userLoginOtpResendApi(requestObject)
  }

  @action.bound
  async userLoginOtpResendApi(
    requestObject: APIPhoneNumber,
    shouldUpdateImmediately: boolean = true
  ) {
    const userOtpResendPromise = this.userAPI.userLoginOtpResendApi(requestObject)
    if (shouldUpdateImmediately) {
      return bindPromiseWithOnSuccess(userOtpResendPromise)
        .to(this.setUserLoginOtpResendStatus, this.resendOtpSuccess)
        .catch((error) => {
          if (__DEV__) console.log(error)
          displayApiError(error)
        })
    }
    return userOtpResendPromise
  }

  @action.bound
  async getUserProfileApi(shouldUpdateImmediately: boolean = true) {
    const profilePromise = this.userAPI.getUserProfileApi()
    if (shouldUpdateImmediately) {
      return bindPromiseWithOnSuccess(profilePromise).to(
        this.setGetProfileStatus,
        this.setUserProfile
      )
    }
    return profilePromise
  }

  @action.bound
  async userLogOutApi(shouldUpdateImmediately: boolean = true) {
    const logoutPromise = this.userAPI.userLogOutApi()
    if (shouldUpdateImmediately) {
      return bindPromiseWithOnSuccess(logoutPromise).to(
        this.setLogOutStatus,
        this.setLogOut
      )
    }
    return logoutPromise
  }

  @action.bound
  async logout() {
    this.clearStore()
    userProfile.clearStore()
    AsyncStorage.clear(() => {
      gotoLaunchScreen({})
    })
  }

  @action.bound
  clearStore() {
    this.userLoginStatus = API_INITIAL
    this.userLoginVerifyStatus = API_INITIAL
    this.userLoginOtpResendStatus = API_INITIAL
    this.getProfileStatus = API_INITIAL
    this.logOutStatus = API_INITIAL
  }
}

const authStore = new AuthStore(UserAPI)
export { authStore as default, AuthStore }
