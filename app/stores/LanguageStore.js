/**
 * @flow
 * @author: Anesh Parvatha
 * */

import { observable, action, computed, reaction } from 'mobx'
import type { APIStatus } from '@rn/api-constants'
import {
  API_INITIAL,
  API_FETCHING,
  API_SUCCESS,
  API_FAILED
} from '@rn/api-constants'

// import { SELECT_LANGUAGE_SCREEN } from '../constants/NavigationConstants'

import getLanguagesData from './data/getLanguages'
import Language from './models/Language'

class LanguageStore {
  @observable languagesMap: ObservableMap<Language>
  @observable getLanguagesAPIStatus: APIStatus

  constructor() {
    this.languagesMap = observable.map({})
    this.getLanguagesAPIStatus = API_INITIAL
  }

  @computed
  get languages() {
    const languages = []
    this.languagesMap.forEach(language => {
      languages.push(language)
    })
    return languages
  }

  @action.bound
  setGetLanguagesAPIStatus(status: APIStatus) {
    this.getLanguagesAPIStatus = status
  }

  @action.bound
  setLanguages(languages: any) {
    languages.forEach(language => {
      const lang = new Language(language)
      if (!this.languagesMap.has(lang.languageId)) {
        this.languagesMap.set(lang.languageId, lang)
      }
    })
  }

  @action.bound
  async getLanguagesAPI() {
    this.setGetLanguagesAPIStatus(API_FETCHING)
    const data = await Promise.resolve(getLanguagesData)
    try {
      this.setGetLanguagesAPIStatus(API_SUCCESS)
      this.setLanguages(data)
    } catch (e) {
      this.setGetLanguagesAPIStatus(API_FAILED)
      console.log('Lang Error: ', e)
    }
  }

  onSelectLanguageScreenOpened() {
    this.getLanguagesAPI()
  }

  // disposeLanguageScreenReaction = reaction(
  //   () => Actions.currentScene,
  //   (scene) => {
  //     if(scene === SELECT_LANGUAGE_SCREEN) {
  //       this.onSelectLanguageScreenOpened()
  //     }
  //   }
  // )
}

const languageStore = new LanguageStore()

export { languageStore as default, LanguageStore }
