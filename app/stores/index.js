/*
 * @flow
 */

import baseAuthStore from './AuthStore/BaseAuthStore'
import authStore from './AuthStore'
import languageStore from './LanguageStore'
import settingsStore from './SettingsStore'

export default {
  baseAuthStore,
  authStore,
  settingsStore,
  languageStore
}
