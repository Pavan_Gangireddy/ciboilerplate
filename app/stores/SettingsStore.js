/**
 * @flow
 * @author: Anesh Parvatha
 * */

import { AsyncStorage } from 'react-native'
import I18n from 'react-native-i18n'

import { observable, action, reaction } from 'mobx'
import { create, persist } from 'mobx-persist'
import {createViewModel} from 'mobx-utils'

import { updateTranslations } from '../I18n/I18n.js'
import { getFormattedLanguage } from '../utils/CommonMethods'


import Language from './models/Language'

const pour = create({
  storage: AsyncStorage,
  jsonify: true
})

class SettingsStore {
  @persist('object')
  @observable
  userLanguage: Language

  constructor() {
    this.userLanguage = new Language({
      language_id: 'en',
      language_label: 'English'
    })
    this.hydrate()
  }

  @action.bound
  createViewModel() {
    const viewModel = createViewModel(this)
    return viewModel
  }

  setUserLanguage(language: Language) {
    this.userLanguage = language
  }

  @action.bound
  hydrate() {
    Object.keys(this).forEach(key => {
      pour(key, this)
    })
  }

  disposeUserLanguage = reaction(
    () => this.userLanguage,
    userLanguage => {
      I18n.locale = getFormattedLanguage(
        userLanguage.languageId
      )
      updateTranslations()
    }
  )
}

const settingsStore = new SettingsStore()
export const settingsStorePromise = Promise.all(
  Object.keys(settingsStore).map((key) => pour(key, settingsStore))
)
export { settingsStore as default, SettingsStore }
