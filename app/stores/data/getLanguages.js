/**
 * @flow
 * @author: Anesh Parvatha
 * */

export function getLanguageById(languageId: string) {
  let usreLanguage = {
    language_id: 'en',
    language_label: 'English'
  }
  languages.forEach(lang => {
    if (languageId === lang.language_id) {
      usreLanguage = lang
    }
  })
  return usreLanguage
}

const languages = [
  {
    language_id: 'en',
    language_label: 'English'
  },
  {
    language_id: 'te',
    language_label: 'తెలుగు'
  },
  {
    language_id: 'hi',
    language_label: 'हिंदी'
  }
  /* ,
  {
    language_id: 'ta',
    language_label: 'தமிழ்'
  },
  {
    language_id: 'kn',
    language_label: 'ಕನ್ನಡ'
  },
  {
    language_id: 'bn',
    language_label: 'বাঙালি'
  },
  {
    language_id: 'mr',
    language_label: 'मराठी'
  } */
]

export default languages
