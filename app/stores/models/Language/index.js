/**
 * @flow
 * @author: Anesh Parvatha
 * */

import { observable } from 'mobx'

class Language {
  @observable languageId: string
  @observable languageLabel: string

  constructor(language) {
    this.languageId = language.language_id
    this.languageLabel = language.language_label
  }
}
export default Language
