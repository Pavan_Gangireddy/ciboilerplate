/**
 * @flow
 * @author: Prasanth
 */


import { observable, action } from 'mobx'
import { create, persist } from 'mobx-persist'
import { AsyncStorage } from 'react-native'

const pour = create({
  storage: AsyncStorage
})

class UserProfile {
  @persist
  @observable
  phoneNumber: string
  @persist
  @observable
  countryCode: string

  @action.bound
  setPhoneNumber(phoneNumber: string) {
    this.phoneNumber = phoneNumber
  }

  @action.bound
  setCountryCode(countryCode: string) {
    this.countryCode = countryCode
  }

  clearStore() {
    this.phoneNumber = ''
    this.countryCode = ''
  }
}

const userProfile = new UserProfile()

export const pourPromise = Promise.all(
  Object.keys(userProfile).map((key) => pour(key, userProfile))
)

export { userProfile as default, UserProfile }
