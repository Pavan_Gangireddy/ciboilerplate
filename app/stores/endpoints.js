/**
 * @flow
 */

const endpoints = {
  auth: {
    userLogin: 'login/v1/',
    verifyLoginOtp: 'login/otp/verify/v1/',
    resendLoginOtp: 'login/otp/resend/v1/',
    userLogout: 'logout/v1/'
  },
  profile: {
    getUserProfile: 'user/profile/v1/'
  }
}

export default endpoints
