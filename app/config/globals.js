// @flow

// Note: Eslint disabled as we want to enforce order of excecution in below order
/* eslint-disable */

import '../components/Common/StyleSheet/config'
import StyleSheet from '../components/Common/StyleSheet'
import AppConfig from '../config/AppConfig'

import { setStyleSheet } from '@rn/styled-components/lib/config'

if (__DEV__) {
  GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest
}

if (AppConfig.allowStyleSheetScaling) {
  setStyleSheet(StyleSheet)
}
