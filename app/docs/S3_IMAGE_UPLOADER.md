This project uses [react-native-image-crop-picker](https://github.com/ivpusic/react-native-image-crop-picker) to pick images and crop images.

We need to follow post installation steps specified in github repo

## Post-install steps

### iOS

#### Step 1

In Xcode open Info.plist and add string key `NSPhotoLibraryUsageDescription` with value that describes why you need access to user photos. More info here https://forums.developer.apple.com/thread/62229. Depending on what features you use, you also may need `NSCameraUsageDescription` and `NSMicrophoneUsageDescription` keys.

#### Step 2

##### Only if you are not using Cocoapods

* Drag and drop the ios/ImageCropPickerSDK folder to your xcode project. (Make sure Copy items if needed IS ticked)
* Click on project General tab
  * Under `Deployment Info` set `Deployment Target` to `8.0`
  * Under `Embedded Binaries` click `+` and add `RSKImageCropper.framework` and `QBImagePicker.framework`

### Android

* Make sure you are using Gradle `2.2.x` (android/build.gradle)

```gradle
buildscript {
    ...
    dependencies {
        classpath 'com.android.tools.build:gradle:2.2.3'
        ...
    }
    ...
}
```

* **VERY IMPORTANT** Add the following to your `build.gradle`'s repositories section. (android/build.gradle)

```gradle
allprojects {
    repositories {
      mavenLocal()
      jcenter()
      maven { url "$rootDir/../node_modules/react-native/android" }

      // jitpack repo is necessary to fetch ucrop dependency
      maven { url "https://jitpack.io" }
    }
}
```

* Add `useSupportLibrary` (android/app/build.gradle)

```gradle
android {
    ...

    defaultConfig {
        ...
        vectorDrawables.useSupportLibrary = true
        ...
    }
    ...
}
```

* [Optional] If you want to use camera picker in your project, add following to `app\src\main\AndroidManifest.xml`
  * `<uses-permission android:name="android.permission.CAMERA"/>`

## Usage

```
  import S3ImageUploader from '..'
  const s3ImageUploader=new S3ImageUploader(pickerOptions)
  s3ImageUploader.uploadImage(getAWSCredsAPI,onUpload)
```

> We can write a wrapper or edit S3ImageUploader project specific instead of passing pickerOptions, getAWSCredsAPI everywhere
