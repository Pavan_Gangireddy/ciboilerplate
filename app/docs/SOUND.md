To use react-native-sound in the project follow these steps


* Add sound files \*.mp3, \*.wav, etc. in to the following path `/packages/native/android/app/src/main/res/raw` in the project
* To use these sound files in the project create constants for sounds where these constants are used as keys in `packages/native/app/themes/Sounds.js`

``SoundConstants.js`` file looks like this:
```
export const RIGHT_ANSWER = 'rightAnswer'
```

``Sounds.js`` file looks like this:


```
import {RIGHT_ANSWER} from '../constants/SoundConstants'
const sounds = [
  {
    url: 'right_answer.mp3',
    key: RIGHT_ANSWER
  }
  ]
 export default sounds
```