# Linking

## Android Installation

### Automatic

```shell
react-native link react-native-analytics
```

(or using [`rnpm`](https://github.com/rnpm/rnpm) for versions of React Native < 0.27)

```shell
rnpm link react-native-analytics
```

### Manual

Add to `anroid/app/src/main/java/com/your-app-name/MainActivity.java`:

```java
import com.facebook.react.ReactPackage;
import com.smore.RNSegmentIOAnalytics.RNSegmentIOAnalyticsPackage; // <-- add this
...

      return Arrays.<ReactPackage>asList(
        new MainReactPackage(),
        new RNSegmentIOAnalyticsPackage(), // <-- add this
        ...
      );
```

## iOS Installation

1.  In your Podfile, add `pod "Analytics"` to your project.
2.  Inside Xcode (make sure you've opened your `.xcworkspace` file), go to the project navigator -> your project's name -> right click `your project's name` -> `Add Files to [your project's name]`.
3.  Go to `node_modules/react-native-analytics/ios` -> and choose the `RNAnalytics` folder.
4.  Make sure you select `RNSegmentIOAnalytics.h` and `RNSegmentIOAnalytics.h` files and add them.

# Code Changes:

1.  In your RootContainer, include the following snippet and respective import statements:

```javascript
import analytics from 'react-native-analytics'
import RNAnalytics from 'react-native-analytics'

// Constants
import AnalyticsConstants from '../../constants/AnalyticsConstants'

...
...
...

@observer
class RootContainer extends Component {
    componentDidMount() {
        // Setup segment analytics
        const flushEverySecondsCount = 1
        RNAnalytics.setup(KEEP_YOUR_SEGMENT_KEY_HERE,flushEverySecondsCount)

        analytics.track(AnalyticsConstants.APP_OPENED, {
            build: '23.0.1' // Build number
        })
    }

    render(){
        return (
            ....
        )
    }
}
```

    NOTE: KEEP_YOUR_SEGMENT_KEY_HERE is enviroment based, use it from EnvironmentConstants file, if you have one.

# Sample Usage:

1.  Wrap every component with WithAnalyticsAPI container, to get analytics as prop
2.  Define event constants in AnalyticsConstants.js file and use them appropriately
3.  For implementation reference, you can look into `AnalyticsSample` component

## Documentation

https://segment.com/docs/libraries/ios/#getting-started
https://segment.com/docs/libraries/android/#getting-started

## For more info:

https://github.com/tonyxiao/react-native-analytics
