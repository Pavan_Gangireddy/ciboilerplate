## Frontend Dev Guidelines

* Communication

  * Inter team - Slack
  * Intra team - Slack; maintain separate channel for each feature team if requires

* Task Completion Checklist

  * Component
  * All component states
    * Handle Loading
    * Handle No data
    * Handle placeholders if any
    * Handle No internet - need to test Try Again
    * Handle dynamic data
    * Handle Swipe refreshing
  * If you are working on reusable Component then write below too
    * Write a story(storybook)
    * Write Comments for props, methods
  * Test cases
  * Write a comment for complex functions, props, etc.
  * Write a postmortem report when required
  * Will be completed when PR merged
  * API Integration

* Project Workflow

  * **NOTE:** Follow `Task Framework` in each & every task
  * Planning
    * Noting down common components
    * Component composition
    * MobX Stores design
    * Test cases design
  * Use bitbucket issues for any problem identified
    * if only one platform module is available/using
  * Daily code reviews
  * Use Storybook

* Code Guidelines
  * Remove `^ and *` from `packages.json`
  * Checklist to use library or a functionality module
    * Is it increasing app starting time?
    * Github issues and stars
    * Performance
    * Informing to team
  * Thematic styles
  * Cycling import check
  * We should rename files or folder only using `git mv fromFile toFile`
  * We should use `Alert.alert()` instead of `alert()`
  * We should destruct the props which will be used multiple times in a component method
  * Flat hierarchy vs Deep hierarchy(Yet to have a discussion)
  * Similar components(Ex: Table) across all the app by one person or so
  * If the computed variable is only related to UI instead of business logic. We need to write a wrapper component with computed variable in the component itself
  * We should write logic in stores only
    * Exceptions -
      * If logic is dealing with UI Components we can write it in Component (Ex : Changing UIComponent based on prop)
  * If we write eslint-disable,eslint-disable-line anywhere in repo we need to mention reason for it
  * Whenever we find an item from array.Before doing operations on that we need to check whether is is undefined or not
    * Exceptions -
      * Static arrays
      * You are very sure about that snippet wont be executed without that value
  * If there is no typeface defined for a Text in zeplin we need to contact RP to add that typeface Text to commons
  * If there is no color defined in style guide in zeplin which we are going to use We need to contact RP to add that color to style guide and Common Colors
  * We should maintain NavigationUtils file which contains methods to navigate to each screen in the app (Ex: goToHomeScreen(){navigate(HOME_SCREEN_CONSTANT)})
  * We should use constants from react-native-config in app/constants/EnvironmentConstants.js only, From there we need to use them.

### Common components and styleguide

* Use StylesSheet from 'app/components/Common/StyleSheet'
* Use Image from 'app/components/Common/Image'
* Use Touchable from 'app/components/Common/Touchable'
* Use LoadingWrapperWithOverlayLoader from 'app/components/Common/LoadingWrapperWithOverlayLoader'
* Use LoadingWrapper from 'app/components/Common/LoadingWrapper'
* Use Texts or Typos from 'app/styledComponents/Text'
* Use Buttons from 'app/styledComponents/Button'

### Checklist before creating PR

* Check output when compared to zeplin in both android and ios
* Should ensure there is no eslint issues
* Remove prop-types, use only flow-types
* Move every string to I18n and follow camelCasing
* Use static default-props, not Component.defaultProps
* Use Constants and Utils folder effectively, where it is necessary
  * Ex: Date formats, change first letter to capitals will be there in utils, do use them
* If any es-lint rule is disabled, explain the reason as comments there itself in code, why it is so
* If es-lint rule is disabled for whole file, explain the reason as comments at the starting of file
* Remove unnecessary code in store (ex: setters, getters, console logs)
* Remove /Unnecessary files/Empty files/Files which are used to experiment/
* Remove /Unnecessary Variables/Empty Variables/Variables which are used to experiment/
* Finally, pull the updated code from the branch and check for the merge conflicts if any and do resolve.
* Ensure above 'common components and styleguide' checklist

### Review Guidlines

* Should ensure 'Checklist before creating PR' checklist
* Reviewer should ensure the output comparing to zeplin.
* Reviewer need to create another branch from reviewing branch if the reviewing branch is already merged, Need to keep PR from new branch to reviewing branch
* Reviewer needs to write tasks in trello board of respective module for UI enhancements or Bugs
