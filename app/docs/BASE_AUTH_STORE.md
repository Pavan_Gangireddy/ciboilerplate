## User Permissions
- Include the below line in AndroidManifest.xml
```
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
```

## Environment Variables
- Define AUTH_BASE_URL, CLIENT_ID, CLIENT_SECRET and SOURCE in Config
