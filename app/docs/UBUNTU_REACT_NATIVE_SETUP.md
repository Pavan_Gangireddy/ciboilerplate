## CURL SETUP

```shell
sudo apt-get install curl
```

## NODE INSTALLATION SETUP

```shell
curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
https://nodejs.org/en/download/package-manager/
```

## JAVA SETUP

```shell
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer
https://medium.com/coderscorner/installing-oracle-java-8-in-ubuntu-16-10-845507b13343
```

## REACTNATIVE

```shell
sudo npm install -g react-native-cli
https://facebook.github.io/react-native/docs/getting-started.html
```

## GIT SETUP

```shell
sudo apt install git-all
```

## YARN SETUP

```shell
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
https://yarnpkg.com/lang/en/docs/install/#debian-stable
```

## ADB SETUP

```shell
sudo atp install adb
```

## USERNAME SETUP

```shell
git config --global user.email "your email"
git config --global user.name "username"
```

## ANDROID STUDIO SETUP

#### Download android studio from the following link : https://developer.android.com/studio/

#### For setting home write this in .bashrc

```shell
export ANDROID_HOME=$HOME/Android/Sdk
```
