# Code Changes:

1.  In your RootContainer, include the following snippet and respective import statements:

```javascript
import Logentries from 'react-native-logentries'
...
...
...

Logentries.setDebugging(true)
Logentries.setToken(LOGIN_ENTRIES_KEY)

@observer
class RootContainer extends Component {
    render(){
        return (
            ....
        )
    }
}
```

    NOTE: LOGIN_ENTRIES_KEY is enviroment based, use it from EnvironmentConstants file, if you have one.

# Sample Usage

1.  To log a network call request,response and url, add the following snippet in the genericNetworkCall
    function used in your project.

```JS
import { logEntriesLog } from '../utils/LogEntriesUtils'

logEntriesLog({
    url,
    requestObject,
    type,
    response
})
```

2.  To log a mobx action or reaction or computation, add the following snippet in app/config/DebugConfig.js.

```JS
import { enableMobxlogEntries } from '../utils/AppUtils'

const config = {
  predicate: () => __DEV__ && Boolean(window.navigator.userAgent),
  action: true,
  reaction: true,
  transaction: true,
  compute: true
}

if (__DEV__) {
  enableLogging(config)
  enableMobxlogEntries(config)
}
```

You can find more information on how to use `react-native-logentries` in [their docs](https://github.com/joelrfcosta/react-native-logentries).
