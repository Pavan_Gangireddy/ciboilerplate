This project uses rn/stylesheet

* Configure the required Height and Width in the ../components/Common/StyleSheet/config.js from zeplin mock of a screen, Default is 360 _ 640 - Width _ Height
* In all places where we have the use case for “Dimensions.get(‘window’)” for width and height, we should use StyleSheet.WIDTH and StyleSheet.HEIGHT.
* For Views or icons with Circular shape, we should use the StyleSheet.getRoundedBorderStyles() to get the modified height, width and borderRadius.
* Wherever we are passing hard coded values for height, width other than in styled components, we should use StyleSheet.getValueBasedOnWidth() and StyleSheet.getValueBasedOnHeight() to get the modified values.
* Stylesheet methods should not be used to compute the dimensions given inside styled components or Stylesheet.create()
