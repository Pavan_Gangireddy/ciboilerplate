## Commands

Automate the creation of components, containers, routes, selectors and sagas -
and their tests - right from the CLI!

Run `npm run generate` in your terminal and choose one of the parts you want
to generate. They'll automatically be imported in the correct places and have
everything set up correctly.

You can also run `npm run generate <part>` to skip the first selection. (e.g. `npm run generate container`)

> We use [plop] to generate new components, you can find all the logic and
> templates for the generation in `internals/generators`.

[plop]: https://github.com/amwmedia/plop
