
Reference:
 - [Building multiple versions of a React Native app](https://medium.com/@ywongcode/building-multiple-versions-of-a-react-native-app-4361252ddde5)
 - [react-native-config](https://github.com/luggit/react-native-config)

# Android

## 1. Adding Product Flavors

Open your project�s **android/app/build.gradle** file, add a new section called ***productFlavors*** and put our flavor specific configuration here. Use a different ***applicationId*** for each flavor so we can have them all install on the device at the same time.

	android {
        compileSdkVersion 26
        buildToolsVersion "26.0.1"
        defaultConfig {...}
        buildTypes {...}
        productFlavors {
            alpha {
				applicationId =  "cricket.ib.CricketLoversCorner.alpha"
				resValue 'string', 'app_name', 'CLC Alpha'
			}

			beta {
				applicationId =  "cricket.ib.CricketLoversCorner.beta"
				resValue 'string', 'app_name', 'CLC Beta'
			}

			gamma {
				applicationId =  "cricket.ib.CricketLoversCorner.gamma"
				resValue 'string', 'app_name', 'CLC Gamma'
			}

			prod {
				applicationId =  "cricket.ib.CricketLoversCorner"
				resValue 'string', 'app_name', 'Cricket Lovers� Corner'
			}
        }
    }

## 2. Config Environment variables specific to the Flavour

Save config for different environments in different files: `.env.alpha`, `.env.prod`, etc.

Define a map in `build.gradle` associating builds with env files. Do it before the `apply from` call, and use build cases in lowercase, like:

```
project.ext.envConfigFiles = [
    alpha: ".env.alpha",
	beta: ".env.beta",
	gamma: ".env.gamma",
	prod: ".env.prod"
]
```

apply the **react-native-config**  after that
```
apply from: project(':react-native-config').projectDir.getPath() + "/dotenv.gradle"
```
finally **android/app/build.gradle** will be like this..
```
project.ext.react = [...]

project.ext.envConfigFiles = [
    alpha: ".env.alpha",
	beta: ".env.beta",
	gamma: ".env.gamma",
	prod: ".env.prod"
]

apply from: project(':react-native-config').projectDir.getPath() + "/dotenv.gradle"

...
```

#iOS Build Targets
 - [Using multiple targets](http://www.folio3.com/blog/using-multiple-targets-in-xcode/)
 - [Multiple targets in iOS](http://blogs.innovationm.com/multiple-targets-in-ios/)
