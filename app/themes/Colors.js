const colors = {
  background: '#1F0808',
  snow: '#fff',
  white: '#fff',
  blackOverlay: 'rgba(0, 0, 0, 0.8)',
  blue: '#00ff',
  lightBlue: '#19c7ff',
  fire: '#e73536',
  imagePlaceholder: '#39435e'
}

export default colors
