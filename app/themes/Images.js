// leave off @2x/@3x
/* eslint-disable global-require */

const images = {
  launch: require('../images/launch-icon.png'),
  ready: require('../images/your-app.png'),
  background: require('../images/BG.png'),
  closeWhite: require('../images/close_white/close.webp')
}

export default images
