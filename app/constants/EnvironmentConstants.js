/**
 * @flow
 * */

// TODO: Need to define AUTH_BASE_URL, CLIENT_ID, CLIENT_SECRET and SOURCE in Config And uncomment Export default config logic

/*
  import Config from 'react-native-config'
  export default Config
*/

const AUTH_BASE_URL = 'https://ib-franchise-backend-alpha.apigateway.in/api/franchise_user/'
const CLIENT_ID = 'franchise_rn_app'
const CLIENT_SECRET = 'franchise_rn_app'
const SOURCE = 'ib-franchise'

export default {
  AUTH_BASE_URL,
  CLIENT_ID,
  CLIENT_SECRET,
  SOURCE
}
