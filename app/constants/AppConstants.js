export const imageUploadStatus = {
  initial: 'INITIAL',
  uploadingStart: 'UPLOADING_START',
  uploadingCompleted: 'UPLOADING_COMPLETED',
  uploadingFailed: 'UPLOADING_FAILED'
}
