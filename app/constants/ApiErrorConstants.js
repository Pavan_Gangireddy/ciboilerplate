/**
 * @flow
 **/


export const INVALID_TOKEN = 'INVALID_TOKEN'
export const INVALID_USER = 'INVALID_USER'
export const TEAM_SELECT_API_FAILED = 417
export const NOT_AUTHORIZED_EXCEPTION = 'NotAuthorizedException'
export const USER_NOT_FOUND_EXCEPTION = 'UserNotFoundException'
export const INVALID_SESSION_TOKEN = 'INVALID_SESSION_TOKEN'
export const REQUEST_TIMED_OUT = 'Endpoint request timed out'
export const INTERNAL_SERVER_ERROR_CODE = 500
export const NO_INTERNET_ERROR_CODE = 503
