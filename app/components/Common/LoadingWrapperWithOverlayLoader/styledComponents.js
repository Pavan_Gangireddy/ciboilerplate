/**
*
* ContestMenu
* @flow
* @author: VaraKumar
*
*/

import styled from '@rn/styled-components/native'

export const OverlayLoaderContainer = styled.View`
  flex: 1;
`
