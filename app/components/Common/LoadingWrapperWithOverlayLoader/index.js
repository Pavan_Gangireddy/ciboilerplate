/*
 * @author: Vara
 * @flow
 */

import React, { Component } from 'react'
import { observer } from 'mobx-react/native'
import { API_FETCHING, API_FAILED } from '@rn/api-constants'

import FailureViewWithOverlay from '../FailureViewWithOverlay'
import { displayApiError } from '../../../utils/CommonMethods'
import Loading from '../../Animations/Loading'

import { OverlayLoaderContainer } from './styledComponents'

export type Props = {
  children: React$Node,
  loadingState?: any,
  renderLoader?: React$Component,
  renderFailure?: React$Component,
  showFailureView?: boolean,
  error?: any,
  onRetry?: Function,
  loaderProps?: Object
}

class LoadingWrapperWithOverlayLoader extends Component<Props> {
  static defaultProps = {
    renderFailure: FailureViewWithOverlay,
    renderLoader: Loading,
    showFailureView: true,
    onRetry: () => {},
    loaderProps: {}
  }

  getErrorObject = () => {
    const { error } = this.props
    const errorDetailObject = displayApiError(error)
    return errorDetailObject
  }
  renderOverlay = () => {
    const {
      loadingState,
      renderLoader: Loader,
      renderFailure: RenderFailure,
      showFailureView,
      onRetry,
      loaderProps
    } = this.props
    if (loadingState === API_FAILED && showFailureView) {
      return (
        <RenderFailure errorObject={this.getErrorObject()} onRetry={onRetry} />
      )
    } else if (loadingState === API_FETCHING) {
      return <Loader {...loaderProps} />
    }
    return null
  }
  render() {
    const { children } = this.props

    return (
      <OverlayLoaderContainer>
        {children}
        {this.renderOverlay()}
      </OverlayLoaderContainer>
    )
  }
}

export default observer(LoadingWrapperWithOverlayLoader)
