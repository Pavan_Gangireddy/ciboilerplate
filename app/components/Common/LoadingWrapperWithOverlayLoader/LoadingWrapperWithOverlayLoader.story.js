import * as React from 'react'
import { Text } from 'react-native'

import { storiesOf } from '@storybook/react-native'

import LoadingWrapperWithOverlayLoader from './index'

storiesOf(
  'LoadingWrapperWithOverlayLoader Stories',
  module
).add('LoadingWrapperWithOverlayLoader', () => (
  <LoadingWrapperWithOverlayLoader loadingState={200}>
    <Text>Children</Text>
  </LoadingWrapperWithOverlayLoader>
))
