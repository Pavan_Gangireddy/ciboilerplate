/**
*
* FailureViewWithBg
* @flow
* @author: Pavan
*
*/

import styled from '@rn/styled-components/native'

export const BackgroundImage = styled.ImageBackground`
  flex: 1;
  align-items: center;
`
