/**
*
* FailureViewWithBg
* @flow
* @author: Pavan
*
*/

import React, { Component } from 'react'
import { Images } from '../../../themes'
import { BackgroundImage } from './styledComponents'
import FailureView from '../FailureView'

type FailureViewWithBgProps = {
  errorObject: any,
  onRetry: Function
}

class FailureViewWithBg extends Component<FailureViewWithBgProps> {
  // eslint-disable-line react/prefer-stateless-function

  render() {
    return (
      <BackgroundImage source={Images.background}>
        <FailureView {...this.props} />
      </BackgroundImage>
    )
  }
}

export default FailureViewWithBg
