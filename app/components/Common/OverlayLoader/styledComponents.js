/**
 * @flow
 * */

import styled from '@rn/styled-components/native'

export const OverlayLoaderContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  flex: 1;
  justify-content: center;
  align-items: center;
  z-index: 10;
`
export const BackgroundBlueContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  flex: 1;
  justify-content: center;
  align-items: center;
  z-index: 10;
`
