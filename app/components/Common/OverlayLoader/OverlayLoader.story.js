import * as React from 'react'

import { storiesOf } from '@storybook/react-native'

import OverlayLoader from './index'

storiesOf('OverlayLoader Stories', module).add('OverlayLoader', () => (
  <OverlayLoader />
))
