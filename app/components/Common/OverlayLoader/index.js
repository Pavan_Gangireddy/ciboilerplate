/**
 * @flow
 * */

import React from 'react'
import { ActivityIndicator } from 'react-native'

import { Colors } from '../../../themes'
import {
  OverlayLoaderContainer,
  BackgroundBlueContainer
} from './styledComponents'

type Props = {
  loaderIsTransparent?: boolean
}

export default class OverlayLoader extends React.Component<Props> {
  static defaultProps = {
    loaderIsTransparent: true
  }

  render() {
    const { loaderIsTransparent } = this.props
    if (loaderIsTransparent === true) {
      return (
        <OverlayLoaderContainer>
          <ActivityIndicator size="large" color={Colors.blue} />
        </OverlayLoaderContainer>
      )
    }
    return (
      <BackgroundBlueContainer>
        <ActivityIndicator size="large" color={Colors.blue} />
      </BackgroundBlueContainer>
    )
  }
}
