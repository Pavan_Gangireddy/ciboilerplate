import * as React from 'react'

import { Text, StyleSheet, View } from 'react-native'
import { storiesOf } from '@storybook/react-native'

import Touchable from './index'


const ORANGE = 'orange'
const YELLOW = 'yellow'

const styles = StyleSheet.create({
  touchableContainerBorderRadius: {
    borderRadius: 5,
    backgroundColor: ORANGE,
    margin: 20
  },
  touchableContainerBorderRadiusSpecifics: {
    borderTopLeftRadius: 5,
    backgroundColor: YELLOW,
    borderBottomRightRadius: 5,
    margin: 20,
    padding: 20,
    alignItems: 'center'
  },
  textStyle: {
    fontSize: 15,
    padding: 20
  }
})

storiesOf('Touchable Stories', module).add('Touchable', () => (
  <Touchable>
    <Text style={styles.textStyle}>Click me</Text>
  </Touchable>
)).add('Touchable Container', () => (
  <Touchable>
    <View style={styles.touchableContainerBorderRadius}>
      <Text style={styles.textStyle}>Click me</Text>
    </View>
  </Touchable>
)).add('Touchable Container Style and Touchable style', () => (
  <Touchable>
    <View style={styles.touchableContainerBorderRadiusSpecifics}>
      <Text >Click me</Text>
      <Text >Click me</Text>
      <Text >Click me</Text>
    </View>
  </Touchable>
))

