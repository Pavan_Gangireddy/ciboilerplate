/**
 * FailureViewWithOverlay
 * @flow
 * @author: Author
 */

import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import FailureViewWithOverlay from './index'

const errorCode = 400
const title = 'This is the error title'
const description = 'This is the error description'
const errorObjectDetails = {
  errorCode,
  title,
  description
}

storiesOf('FailureViewWithOverlay Stories', module).add('FailureViewWithOverlay', () => (
  <FailureViewWithOverlay errorObject={errorObjectDetails} />
))
