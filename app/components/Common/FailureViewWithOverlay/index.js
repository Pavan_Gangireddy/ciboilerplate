/**
*
* FailureViewWithOverlay
* @flow
* @author: Pavan
*
*/

import React, { Component } from 'react'
import { GradientContainer } from './styledComponents'
import FailureView from '../FailureView'

type FailureViewWithOverlayProps = {
  errorObject: any,
  onRetry: Function
}

class FailureViewWithOverlay extends Component<FailureViewWithOverlayProps> {
  // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <GradientContainer>
        <FailureView {...this.props} />
      </GradientContainer>
    )
  }
}

export default FailureViewWithOverlay
