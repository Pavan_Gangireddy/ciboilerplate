/**
 *
 * FailureViewWithOverlay
 * @flow
 * @author: Pavan
 *
 */

import styled from '@rn/styled-components/native'
import { Colors } from '../../../themes'

export const GradientContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-left: 15;
  padding-right: 15;
  background-color: ${Colors.blackOverlay};
`
