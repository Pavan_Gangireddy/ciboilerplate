/*
 * @flow
 * @author: Chinmaya
 */

import * as React from 'react'
import { observer } from 'mobx-react/native'

import {
  API_FETCHING,
  API_FAILED,
  API_SUCCESS,
  API_SWIPE_REFRESH
} from '@rn/api-constants'

import OverlayLoader from '../OverlayLoader'
import FailureViewWithBg from '../FailureViewWithBg'
import FailureViewWithOverlay from '../FailureViewWithOverlay'
import Loading from '../../Animations/Loading'
import LoadingWithBG from '../../Animations/LoadingWithBG'
import { displayApiError } from '../../../utils/CommonMethods'

type Props = {
  loadingState?: any,
  renderFailure?: any,
  renderLoader?: any,
  error?: any,
  onRetry: Function,
  containerWrappedWithAbsoluteModal?: boolean
}

@observer
export class LoadingWrapper extends React.Component<Props> {
  static defaultProps = {
    renderFailure: FailureViewWithBg,
    renderLoader: OverlayLoader,
    error: undefined,
    onRetry: () => {},
    containerWrappedWithAbsoluteModal: false
  }

  getErrorObject = () => {
    const { error } = this.props
    const errorDetailObject = displayApiError(error)
    return errorDetailObject
  }

  renderFailure = () => {
    const { renderFailure: RenderFailure, onRetry } = this.props
    // NOTE: FailureViewWithOverlay is used for failure view for a component wrapped
    // with ModalContainer Absolute view
    if (this.props.containerWrappedWithAbsoluteModal) {
      return (
        <FailureViewWithOverlay
          onRetry={onRetry}
          errorObject={this.getErrorObject()}
        />
      )
    }
    return (
      <RenderFailure onRetry={onRetry} errorObject={this.getErrorObject()} />
    )
  }

  renderLoader = () => {
    // NOTE: Loading component is used for loading view for a component wrapped
    // with ModalContainer Absolute view
    if (this.props.containerWrappedWithAbsoluteModal) {
      return <Loading />
    }
    return <LoadingWithBG />
  }

  render() {
    const { loadingState, children } = this.props
    switch (loadingState) {
      case API_FETCHING:
        return this.renderLoader()
      case API_SUCCESS:
      case API_SWIPE_REFRESH:
        return children
      case API_FAILED:
        return this.renderFailure()
      default:
        return null
    }
  }
}
export default LoadingWrapper
