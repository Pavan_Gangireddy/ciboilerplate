import * as React from 'react'
import { Text } from 'react-native'
import { storiesOf } from '@storybook/react-native'

import LoadingWrapper from './index'

storiesOf('LoadingWrapper Stories', module).add('LoadingWrapper', () => (
  <LoadingWrapper loadingState={200}>
    <Text>Children</Text>
  </LoadingWrapper>
))
