/**
 * @flow
 * Author: Aravind
 * */

import RNStyleSheet from '@rn/stylesheet'

export const StyleSheet = RNStyleSheet

export default StyleSheet
