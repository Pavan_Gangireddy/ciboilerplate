/**
*
* ErrorMessage
* @flow
* @author: Pavan
*
*/

import styled from '@rn/styled-components/native'

import Image from '../../Common/Image'
import Button from '../../../styledComponents/Button'
import { Typo18W } from '../../../styledComponents/Text'

export const MainContainer = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding-horizontal: 10;
`

export const ErrorTitle = Typo18W.extend`
  text-align: center;
  margin-top: 18;
`

export const ErrorDescriptionText = Typo18W.extend`
  text-align: center;
  opacity: 0.6;
  margin-bottom: 30;
  line-height: 21;
`

export const ErrorRetryButton = styled(Button)`
  justify-content: center;
  width: 130;
`

export const NoWifiErrorImage = styled(Image)`
  width: 57;
  height: 56;
`

export const OopsErrorImage = styled(Image)``
