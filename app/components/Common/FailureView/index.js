/**
 *
 * ErrorMessage
 * @flow
 * @author: Pavan
 *
 */

import React, { Component } from 'react'
import { Actions } from 'react-native-router-flux'
import I18n from 'react-native-i18n'

import { Images } from '../../../themes'
import {
  INTERNAL_SERVER_ERROR_CODE,
  NO_INTERNET_ERROR_CODE
} from '../../../constants/ApiErrorConstants'

import {
  MainContainer,
  ErrorTitle,
  ErrorDescriptionText,
  ErrorRetryButton,
  NoWifiErrorImage,
  OopsErrorImage
} from './styledComponents'

type FailureViewProps = {
  errorObject: any,
  onRetry: Function
}

class FailureView extends Component<FailureViewProps> {
  // eslint-disable-line react/prefer-stateless-function

  onGoBackPress = () => {
    Actions.pop()
  }

  getOnPressFunction = errorCode => {
    const { onRetry } = this.props
    if (
      Actions &&
      Actions.state &&
      Actions.state.routes.length != 0 &&
      Actions.state.routes[0] &&
      Actions.state.routes[0].index === 0
    ) {
      return onRetry
    }
    if (errorCode === INTERNAL_SERVER_ERROR_CODE) {
      return this.onGoBackPress
    }
    return onRetry
  }

  getButtonText = errorCode => {
    if (
      Actions &&
      Actions.state &&
      Actions.state.routes.length != 0 &&
      Actions.state.routes[0] &&
      Actions.state.routes[0].index === 0
    ) {
      return I18n.t('errorScreen.retry')
    }
    if (errorCode === INTERNAL_SERVER_ERROR_CODE) {
      return I18n.t('errorScreen.goBack')
    }
    return I18n.t('errorScreen.retry')
  }

  // TODO: Render the Error image based on the error code
  render() {
    const { errorObject } = this.props
    const { title, description, errorCode } = errorObject
    return (
      <MainContainer>
        {errorCode === NO_INTERNET_ERROR_CODE ? (
          <NoWifiErrorImage source={Images.noWifiImage} />
        ) : (
          <OopsErrorImage source={Images.oopsImage} />
        )}
        <ErrorTitle>{title}</ErrorTitle>
        {description ? (
          <ErrorDescriptionText>{description}</ErrorDescriptionText>
        ) : null}
        <ErrorRetryButton
          onPress={this.getOnPressFunction(errorCode)}
          title={this.getButtonText(errorCode)}
        />
      </MainContainer>
    )
  }
}

export default FailureView
