/**
 * @flow
 * Author: Sanyasirao Mopdada
 **/

import RNImage from '@rn/image'
import React, { Component } from 'react'
import { observer } from 'mobx-react/native'

@observer
class Image extends Component {
  render() {
    return <RNImage {...this.props} />
  }
}

export default Image
