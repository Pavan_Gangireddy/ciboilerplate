/**
 *
 * IplIcons
 * @flow
 * @author: Manikanta
 *
 */
/*eslint-disable*/
import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import styled from '@rn/styled-components/native'
import Image from './index'

const ImageWrapper = styled(Image)`
  width: 150;
  height: 150;
`

storiesOf('Image Placeholder', module)
  .add('Image', () => (
    <ImageWrapper
      addScaling={false}
      source={{
        uri:
          'https://d18vlro5mw8g66.cloudfront.net/alpha/media/contest_images/ipl_context_text.webp'
      }}
    />
  ))
  .add('Image with placeholder', () => (
    <ImageWrapper
      hasPlaceholder
      addScaling={false}
      source={{
        uri:
          'https://d18vlro5mw8g66.cloudfront.net/alpha/media/contest_images/ipl_context_text.webp'
      }}
    />
  ))
  .add('Image with placeholder and custom styles to placeholders', () => (
    <ImageWrapper
      hasPlaceholder
      placeholderColor="red"
      style={{ width: 100, height: 100, borderRadius: 50 }}
      addScaling={false}
      source={{
        uri:
          'https://d18vlro5mw8g66.cloudfront.net/alpha/media/contest_images/ipl_context_text.webp'
      }}
    />
  ))
