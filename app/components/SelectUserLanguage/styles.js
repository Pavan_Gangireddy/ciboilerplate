/**
 * @flow
 * @author: Anesh Parvatha
 * */

import StyleSheet from '../../components/Common/StyleSheet'

import Colors from '../../themes/Colors'

const styles = StyleSheet.create({
  activeItemStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Colors.lightBlue
  },
  activeItemTextStyles: {
    color: Colors.white
  }
})

export default styles
