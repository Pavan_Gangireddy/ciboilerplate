/**
 * @flow
 * @author: Anesh Parvatha
 * */

import styled from '@rn/styled-components/native'

import Colors from '../../themes/Colors'

import Image from '../Common/Image'


export const SelectLanguageTitle = styled.Text`
  margin-left: 20;
  margin-bottom: 14;
  font-weight: normal;
`

export const LanguageText = styled.Text``

export const SelectLanguageContainer = styled.View`
  border-radius: 14;
  background-color: ${Colors.white};
  padding-top: 16;
  padding-bottom: 15;
  width: 320;
`

export const LanguageItem = styled.View`
  padding-top: 12;
  padding-bottom: 12;
  padding-left: 30;
  padding-right: 25;
`

