/**
 * @flow
 * @author: Anesh Parvatha
 * */

import React, { Component } from 'react'
import { observer } from 'mobx-react/native'
import I18n from 'react-native-i18n'

import Touchable from '../Common/Touchable'

import Images from '../../themes/Images'

import {
  SelectLanguageContainer,
  SelectLanguageTitle,
  LanguageItem,
  LanguageText,
  TickImage
} from './styledComponents'
import styles from './styles'

type Props = {
  style: number,
  selectedLanguage: string
}

type InjectedProps = {}

type ComponentProps = Props & InjectedProps

function LanguageListItem(props: any) {
  const { language, selected, onSelectLanguage } = props
  const { languageLabel } = language
  let itemTextStyles = {}
  let itemStyles = {}
  if (selected) {
    itemStyles = styles.activeItemStyles
    itemTextStyles = styles.activeItemTextStyles
  }
  const onChooseLang = () => {
    onSelectLanguage(language)
  }
  return (
    <Touchable onPress={onChooseLang}>
      <LanguageItem style={itemStyles}>
        <LanguageText style={itemTextStyles}>{languageLabel}</LanguageText>
      </LanguageItem>
    </Touchable>
  )
}

@observer
class SelectUserLanguage extends Component<ComponentProps> {
  static defaultProps = {
    style: 0
  }
  renderLanguageList = (props: Props) => {
    const { languageStore, onSelectLanguage, selectedLanguage } = props
    const { languages } = languageStore
    return languages.map(language => (
      <LanguageListItem
        language={language}
        selected={selectedLanguage === language.languageId}
        onSelectLanguage={onSelectLanguage}
        key={language.languageId}
      />
    ))
  }
  render() {
    const { style } = this.props
    const title = I18n.t('selectLanguageScreen.selectLanguage', {
      locale: this.props.selectedLanguage
    })
    return (
      <SelectLanguageContainer style={style}>
        <SelectLanguageTitle>{title}</SelectLanguageTitle>
        {this.renderLanguageList(this.props)}
      </SelectLanguageContainer>
    )
  }
}
export default SelectUserLanguage
