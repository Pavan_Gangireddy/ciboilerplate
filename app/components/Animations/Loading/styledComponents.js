/**
 *
 * FlipQuestion
 * @flow
 * @author: Maneesh
 *
 */

import styled from '@rn/styled-components/native'

import { Colors } from '../../../themes'

export const MainContainer = styled.View`
  background-color: ${Colors.blackOverlay};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  flex: 1;
  justify-content: center;
  align-items: center;
  z-index: 10;
`
