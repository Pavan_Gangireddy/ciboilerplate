/**
 *
 * AddPoints
 * @flow
 * @author: Janardhan
 *
 */
import React, { Component } from 'react'
import type { StyleObj } from 'react-native/Libraries/StyleSheet/StyleSheetTypes'
import { ActivityIndicator } from 'react-native'

import { Colors } from '../../../themes'
import { MainContainer } from './styledComponents'

type Props = {
  style?: StyleObj
}

export default class Loading extends Component<Props> {
  render() {
    const { style } = this.props
    return (
      <MainContainer style={style}>
        <ActivityIndicator size="large" color={Colors.white} />
      </MainContainer>
    )
  }
}
