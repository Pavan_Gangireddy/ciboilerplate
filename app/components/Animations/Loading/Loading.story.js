/**
*
* Loading
* @flow
* @author: Maneesh
*
*/

import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import Loading from './index'

storiesOf('Loading Stories', module).add('Loading', () => <Loading />)
