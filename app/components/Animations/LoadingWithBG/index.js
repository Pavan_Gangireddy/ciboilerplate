/**
 *
 * LoadingWithBG
 * @flow
 * @author: Aravind
 *
 */
import React, { Component } from 'react'
import { ActivityIndicator } from 'react-native'

import { BackgroundImage } from './styledComponents'
import { Images, Colors } from '../../../themes'

export default class LoadingWithBG extends Component {
  render() {
    return (
      <BackgroundImage source={Images.background}>
        <ActivityIndicator size="large" color={Colors.white} />
      </BackgroundImage>
    )
  }
}
