/**
*
* @flow
* @author: Manikanta
*
*/

import React, { Component } from 'react'
import { Text } from 'react-native'
import { InfoContainer } from './styledComponents'

const colors = ['#382c40e6', '#0e2239e6']

class LinearGradient extends Component {
  render() {
    return (
      <InfoContainer colors={colors}>
        <Text>Sample</Text>
      </InfoContainer>
    )
  }
}

export default LinearGradient
