/**
*
* @flow
* Author: Manikanta
*
*/

import styled from '@rn/styled-components/native'
import LinearGradient from 'react-native-linear-gradient'

export const InfoContainer = styled(LinearGradient)`
  padding-top: 1.5;
  padding-bottom: 1.5;
  padding-left: 5;
  padding-right: 5;
  flex-direction: row;
  justify-content: space-around;
  border-radius: 50;
  margin-left: 17;
  margin-top: -5;
  width: 60;
`
