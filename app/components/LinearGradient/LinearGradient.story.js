/**
 *
 * @flow
 * @author: Manikanta
 *
 */
/*eslint-disable*/
import * as React from 'react'
import { storiesOf } from '@storybook/react-native'
import LinearGradient from './index'


storiesOf('LinearGradient', module)
  .add('LinearGradient Sample', () => (
    <LinearGradient />
  ))

