#!/bin/bash

# Colourise the output
RED='\033[0;31m'        # Red
GRE='\033[0;32m'        # Green
YEL='\033[1;33m'        # Yellow
NCL='\033[0m'           # No Color

# File extensions whitelist - to be processed
declare -a allowed_files=("png" "jpg")

# File extensions whitelist - to be ignored
declare -a ignored_files=("webp" "svg" "js")

# Webp conversion options
QUALITY=90

check_requirements() {
  command -v cwebp >/dev/null 2>&1 || { echo "${RED}This script requires cwebp but it's not installed.${NCL} \nMac: http://brewformulas.org/Webp\nUbuntu: 'sudo apt install webp'\nAborting." >&2; exit 1; }
}

contains() {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 1; done
  return 0
}

spinner() {
  pid=$! # Process Id of the previous running command
  spin='-\|/'

  i=0
  while kill -0 $pid 2>/dev/null
  do
    i=$(( (i+1) %4 ))
    printf "\r${spin:$i:1} Converting images to webp..."
    sleep .1
  done
}

convert_to_webp() {
  entry=$1
  quality=$2

  # echo "${GRE}[ + ] Converting ${entry}"

  file_name="$(basename "${entry}")"
  dir="$(dirname "${entry}")/"
  name="${file_name%.*}"
  ext=".webp"

  output_image=$dir$name$ext
  # cwebp <input_image> -q 90 -o <output_image>
  cwebp "${entry}" -q $quality -o "${output_image}" >/dev/null 2>&1
  rm "${entry}"
}



process_file() {
  # printf "${GRE}%s${NCL}\n" "${entry}"

  quality=$1

  file_name="$(basename "${entry}")"
  dir="$(dirname "${entry}")"
  name="${file_name%.*}"
  ext="${file_name##*.}"
  # size="$(du -sh "${entry}" | cut -f1)"

  contains "${ext}" "${allowed_files[@]}"
  is_allowed_file=$?
  if [ $is_allowed_file == 1 ]
  then
    convert_to_webp "${entry}" $quality
  else
    # echo "${RED}[ - ] Removing ${entry}"
    contains "${ext}" "${ignored_files[@]}"
    is_ignored_file=$?
    if [ $is_ignored_file == 0 ]
    then
      rm "${entry}"
    fi
  fi
}

convert() {
  # If the entry is a file then convert it to webp
  for entry in "$1"/*; do [[ -f "$entry" ]] && process_file $2; done

  # TODO: If entire folder doesn't have image files then we should ignore the folder

  # If the entry is a directory call convert() == create recursion
  for entry in "$1"/*; do [[ -d "$entry" ]] && convert "$entry" $2; done
}

if [ "$1" == "-h" ]; then
  echo "Usage: `basename $0` - Program convert to png images to webp images
    ${YEL}Command:${GRE} sh convert-to-webp.sh <input_folder_path> <output_folder_path> <quality>${NCL}
    where
      ${YEL}input_folder_path:${NCL} folder which contains png files
      ${YEL}output_folder_path:${NCL} folder which will contain converted webp files
      ${YEL}quality:${NCL} webp conversion quality option. Ex: 90
  "
  exit 0

fi

check_requirements

# If the path is empty use the current, otherwise convert relative to absolute; Exec convert()
[[ -z "${1}" ]] && ABS_PATH="${PWD}" || cd "${1}" && ABS_PATH="${PWD}"
inputPath=${1:-"${PWD}"}
outputPath=${2:-"${HOME}/webp"}
quality=${3:-${QUALITY}}

cp -R $inputPath $outputPath
convert "${outputPath}" $quality &
spinner
echo "${GRE}\rCheck generated files in ${outputPath}"


