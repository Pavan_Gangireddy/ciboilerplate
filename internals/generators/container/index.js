/**
 * Container Generator
 */

const componentExists = require('../utils/componentExists')

module.exports = {
  description: 'Add a container component',
  prompts: [
    {
      type: 'list',
      name: 'type',
      message: 'Select the base component type:',
      default: 'Stateless Function',
      choices: () => ['Stateless Function', 'PureComponent', 'Component']
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Form',
      validate: value => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? 'A component or container with this name already exists'
            : true
        }

        return 'The name is required'
      }
    },
    {
      type: 'input',
      name: 'developerName',
      message: 'Who are you?',
      default: 'iB Hubs'
    },
    {
      type: 'confirm',
      name: 'wantNavigationUtil',
      message: 'Do you want wantNavigationUtil?',
      default: true
    },
    {
      type: 'confirm',
      name: 'wantStorybook',
      default: true,
      message: 'Do you want wantStorybook?'
    }
  ],
  actions: data => {
    // Generate index.js and index.test.js
    var componentTemplate // eslint-disable-line no-var

    switch (data.type) {
      case 'Stateless Function': {
        componentTemplate = './container/stateless.js.hbs'
        break
      }
      default: {
        componentTemplate = './container/class.js.hbs'
      }
    }

    const actions = [
      {
        type: 'add',
        path: '../../app/containers/{{properCase name}}/index.js',
        templateFile: componentTemplate,
        abortOnFail: true
      },
      {
        type: 'add',
        path:
          '../../app/containers/{{properCase name}}/{{properCase name}}.test.js',
        templateFile: './common/test.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../../app/containers/{{properCase name}}/styledComponents.js',
        templateFile: './common/styledComponents.js.hbs',
        abortOnFail: true
      }
    ]
    // If they want a storybook file
    if (data.wantStorybook) {
      actions.push({
        type: 'add',
        path:
          '../../app/containers/{{properCase name}}/{{properCase name}}.story.js',
        templateFile: './common/storybook.js.hbs',
        abortOnFail: true
      })
    }

    if (data.wantNavigationUtil) {
      actions.push({
        type: 'modify',
        path: '../../app/utils/NavigationUtils.js',
        pattern: /$(?![\r\n])/gi,
        templateFile: './container/navigationUtil.js.hbs',
        abortOnFail: true
      })
      actions.push({
        type: 'modify',
        path: '../../app/utils/NavigationUtils.js',
        pattern: /(\} from '..\/constants\/NavigationConstants')/gi,
        templateFile: './container/partials/navigationConstantWithComa.js.hbs',
        abortOnFail: true
      })
      actions.push({
        type: 'append',
        path: '../../app/constants/NavigationConstants.js',
        pattern: /$(?![\r\n])/gi,
        templateFile: './container/navigationConstant.js.hbs',
        abortOnFail: true
      })
      actions.push({
        type: 'modify',
        path: '../../app/navigation/NavigationRouter.js',
        pattern: /(\} from '..\/constants\/NavigationConstants')/gi,
        templateFile: './container/partials/navigationConstantWithComa.js.hbs',
        abortOnFail: true
      })
      actions.push({
        type: 'modify',
        path: '../../app/navigation/NavigationRouter.js',
        pattern: /(<Stack key="InitialStack">)/gi,
        templateFile: './container/partials/appendSceneInRouter.js.hbs',
        abortOnFail: true
      })
      actions.push({
        type: 'append',
        path: '../../app/navigation/NavigationRouter.js',
        pattern: /(\} from 'react-native-router-flux')/gi,
        templateFile: './container/partials/importContainerStatement.js.hbs',
        abortOnFail: true
      })
    }
    return actions
  }
}
