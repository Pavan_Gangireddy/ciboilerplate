/**
 * Component Generator
 */

/* eslint strict: ["off"] */

'use strict'

const componentExists = require('../utils/componentExists')

module.exports = {
  description: 'Add an unconnected component',
  prompts: [
    {
      type: 'list',
      name: 'type',
      message: 'Select the type of component',
      default: 'Stateless Function',
      choices: () => ['Stateless Function', 'PureComponent', 'Component']
    },
    {
      type: 'input',
      name: 'name',
      message: 'What should it be called?',
      default: 'Button',
      validate: value => {
        if (/.+/.test(value)) {
          return componentExists(value)
            ? 'A component or container with this name already exists'
            : true
        }

        return 'The name is required'
      }
    },

    {
      type: 'input',
      name: 'developerName',
      message: 'Who are you?',
      default: 'iB Hubs'
    },

    {
      type: 'confirm',
      name: 'wantStorybook',
      default: true,
      message: 'Do you want wantStorybook?'
    }
  ],
  actions: data => {
    // Generate index.js and index.test.js
    let componentTemplate

    switch (data.type) {
      case 'Stateless Function': {
        componentTemplate = './component/stateless.js.hbs'
        break
      }
      default: {
        componentTemplate = './component/class.js.hbs'
      }
    }

    const actions = [
      {
        type: 'add',
        path: '../../app/components/{{properCase name}}/index.js',
        templateFile: componentTemplate,
        abortOnFail: true
      },
      {
        type: 'add',
        path:
          '../../app/components/{{properCase name}}/{{properCase name}}.test.js',
        templateFile: './common/test.js.hbs',
        abortOnFail: true
      },
      {
        type: 'add',
        path: '../../app/components/{{properCase name}}/styledComponents.js',
        templateFile: './common/styledComponents.js.hbs',
        abortOnFail: true
      }
    ]

    // If they want a storybook file
    if (data.wantStorybook) {
      actions.push({
        type: 'add',
        path:
          '../../app/components/{{properCase name}}/{{properCase name}}.story.js',
        templateFile: './common/storybook.js.hbs',
        abortOnFail: true
      })
    }
    return actions
  }
}
